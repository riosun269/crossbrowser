package parallelTest;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import org.testng.annotations.Test;

import org.testng.annotations.Parameters;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class CrossBrowserScript {

	WebDriver driver;
	/**
	 * This function will execute before each Test tag in testng.xml
	 * @param browser
	 * @throws Exception
	 */
	@BeforeTest
	@Parameters ("browser")
	  public void setup(String browser) throws Exception{
		//check if parameter passed from TestNG is 'firefox'
		if(browser.equalsIgnoreCase("firefox")) {
			//create firefox instance
			System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		
		//check if is 'chrome'
		else if(browser.equalsIgnoreCase("chrome")) {
			//create chrome instance
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		//Check if parameter passed as Edge
		else if(browser.equalsIgnoreCase("Edge")) {
			System.setProperty("webdriver.ie.driver", "C:\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		else {
			//if no browser passed
			throw new Exception("Browser is not corrrect");
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		  
	  }
	
  @Test
  public void testParameterWithXML() throws InterruptedException {
	  
	  driver.get("http://demo.guru99.com/V4");
	  //Find user name
	  WebElement username = driver.findElement(By.name("uid"));
	  username.sendKeys("guru99");
	  //Find password
	  WebElement password = driver.findElement(By.name("password"));
	  //Fill password
	  password.sendKeys("guru99");
	  
	  System.out.println("Input username and password successfully!");
	  
	  
  }
  
  

  @AfterTest
  public void afterTest() {
  }

}
